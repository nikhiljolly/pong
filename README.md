# Pong

A local multiplayer pong game made with Unity using Unet.

# Basic multiplayer workflow:

1.  **Hosting the game**: The main menu has two buttons to host a game. Host 1 and 2 for hosting level 1 and 2 respectively. On hosting a game a broadcast is sent (using Network Discovery's StartAsServer) to all the clients on the local network with the level and user name as broadcast data. Also the actual game is hosted (using Network Manager's StartHost).
2.  **Joining the game**: When a broadcast is received a join button is added to the empty scroll list on the main menu, with the apt game name. On clicking the join button the user stops receiving broadcast data and joins the respective game (using Network Manager's StartClient) whilst instantiating the apt level.
3.  When the 2nd player is active on the server (host), it sends an RPC to all the clients to **actually start the game** (inside PaddleController).
4.  **Gameplay**: Briefly each player sends their own input for the paddle, to the server. And the server authoritates all the movements and behaviour. The paddle movement & power-up's are synced via a simple SyncVar, whereas the ball's & artifacts' movements are synced to the clients using the inbuilt NetworkTransform. 
5.  The score is also managed via a SyncVar and a custom hook. When the score reaches the winning score, each client shows a gameover screen with the text telling who won. A restart button is also shown, which for now just shutdowns the network manager whilst destroying it and restarting the whole game.

**Note**: Two debug buttons are preset in the game, on the left and right bottom of the screen. The blue one restarts the game whilst shutting down the network manager, while the red when clears the player prefs (username) and restarts the game. A debug text is also present which can be used to show any text via UI Manager.

# Important improvements:

1.  Ball and other Network Transform movements: Currently I am using the network transform for syncing movements on server with the clients, which is far from the best way to do things.
2.  No fallbacks incase a player leaves the game. Although one could restart easily using the debug buttons.
3.  Possible bug in teleportation in a certain scenario.