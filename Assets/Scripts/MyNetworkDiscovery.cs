﻿using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Custom network discovery class to host and join a game; and to receive broadcasts.
/// </summary>
public class MyNetworkDiscovery : NetworkDiscovery{
     
    #region Variables

    //Private
    MyNetworkManager networkManager;

    #endregion

    void Awake(){
        //Initialize and start looking for games to join
        base.Initialize();
        base.StartAsClient();
    }

    void Start(){
        networkManager = GameManager.manager.networkManager;
    }

    /// <summary>
    /// Start the game and start broadcasting - so that another player can join.
    /// </summary>
    /// <param name="lvl">The level to start.</param>
    /// <param name="gameName">The game name to be passed as broadcast data.</param>
    public void StartBroadcastingAndHost(int lvl, string gameName){
        base.StopBroadcast();
        base.Initialize();

        //Update gameName, level and port no as data in network discovery for the broadcast
        broadcastData = lvl + "-" + gameName + ":7777";

        //Start broadcasting
        base.StartAsServer();

        //Host a game
        networkManager.StartHost();
    }

    /// <summary>
    /// Join the game, the network address, port and level details were updated by an intial broadcast.
    /// </summary>
    public void JoinGame(){
        base.StopBroadcast();
        networkManager.StartClient();
    }

    public override void OnReceivedBroadcast(string fromAddress, string data){

        base.OnReceivedBroadcast(fromAddress, data);

        //Note: data contains user name and port number

        // UIManager.manager.Debug_Text("OnReceivedBroadcast: " + fromAddress + " / " + data);

        string address = fromAddress.Split(':')[3];
        int port = int.Parse(data.Split(':')[1]);

        networkManager.networkAddress = address;
        networkManager.networkPort = port;

        //Add available game button with text in data as the game name, also contains the level no
        UIManager.manager.AddAvailableGame(data.Split(':')[0]);

    }

}
