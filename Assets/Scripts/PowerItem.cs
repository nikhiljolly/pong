﻿using Pong;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// A power item that can be picked up by the ball.
/// </summary>
public class PowerItem : NetworkBehaviour{
    /// <summary>
    /// Currently active power item in the game, only one power item is collectable for now.
    /// </summary>
    public static PowerItem activePowerItem;

    #region Variables

    /// <summary>
    /// The type of power this item has.
    /// SyncVar with hook UpdatePower
    /// </summary>
    [SyncVar(hook = "UpdatePower")]
    public Power powerType;

    /// <summary>
    /// The colors associated with the powers, assigned in the inspector.
    /// </summary>
    public Color[] colors;

    #endregion

    #region Unity Defaults

    [ServerCallback]
    void Awake(){
        //Currently active power item, we want only one power item to exist, for now
        activePowerItem = this;

        //Select a random power (but !none i.e. !0)
        powerType = (Power)Random.Range(1, 4);

        //Set color based on power type
        GetComponent<SpriteRenderer>().color = colors[(int)powerType];
    }

    #endregion

    #region Hooks

    /// <summary>
    /// Hook called on clients when power p is changed on the server.
    /// Changes the color for better recognition of power.
    /// </summary>
    /// <param name="p"></param>
    public void UpdatePower(Power p){
        powerType = p;

        //Set color based on power type
        GetComponent<SpriteRenderer>().color = colors[(int)p];
    }

    #endregion

    #region External

    /// <summary>
    /// Picked by the ball, with the knowledge of last paddle that hit the ball.
    /// Inturn the paddle on that side gets the power up.
    /// Called by the ball on the server.
    /// </summary>
    /// <param name="paddleSide"></param>
    [Server]
    public void PickedUp(int paddleSide){
        //print("PickedUp " + name + " by " + paddleSide);

        //Power up the respective paddle with the power type
        GameManager.manager.paddles[paddleSide].SetPower(powerType);

        //Destroy the power item
        NetworkServer.Destroy(gameObject);
        //Allows another one to be spawned, TODO not required?
        activePowerItem = null;
    }

    /// <summary>
    /// Instead of spawning another power item, replace the previous one with a new random one
    /// </summary>
    [Server]
    public void Regenerate(){
        //Select a random power (but !none i.e. !0)
        powerType = (Power)Random.Range(1, 4);

        //Set color based on power type
        GetComponent<SpriteRenderer>().color = colors[(int)powerType];
    }

    #endregion

}
