﻿using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Destroy the object if it exits the exit walls.
/// Server authoritative, the object is destroyed via the server.
/// </summary>
public class DestroyOnExit : NetworkBehaviour{

    #region Physics Defaults

    //Server authoritative, the object is destroyed via the server.
    [ServerCallback]
    void OnTriggerExit2D(Collider2D other){
        switch (other.tag){
            case "Wall":

                NetworkServer.Destroy(gameObject);

                break;
        }
    }

    #endregion

}
