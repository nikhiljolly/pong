﻿namespace Pong{
    //Various enums used in the game, for ease of access added to a namespace.

    //Game Management:

    /// <summary>
    /// The state of the game.
    /// </summary>
    public enum State{
        Menu,
        Waiting,
        InGame,
        Over
    }

    //Paddle:

    /// <summary>
    /// Various powers that a paddle or a power item can have.
    /// </summary>
    public enum Power{
        none,
        increaseSize,
        increaseHitPower,
        makeSticky
    }
}