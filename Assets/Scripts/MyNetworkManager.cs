﻿using UnityEngine;
using UnityEngine.Networking;

public class MyNetworkManager : NetworkManager{

    public override void OnServerConnect(NetworkConnection conn){
        // UIManager.manager.Debug_Text("OnPlayerConnected: " + conn.connectionId);

        // 2nd player connected
        if (conn.connectionId == 1){
            //Stop broadcasting
            GameManager.manager.networkDiscovery.StopBroadcast();
            //Change Waiting UI text
            UIManager.manager.WaitingText("Starting the game..");
        }
    }

}
