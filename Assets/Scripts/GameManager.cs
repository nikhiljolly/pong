﻿using Pong;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class GameManager : MonoBehaviour{

    #region Variables

    public static GameManager manager;

    /// <summary>
    /// Player's username and opponent's name.
    /// 
    /// Player's name is loaded from input or playerprefs.
    /// And opponent's name is updated by the opp on joining the game.
    /// </summary>
    public string userName, oppName;

    /// <summary>
    /// The score required to win the game
    /// </summary>
    public int winScore;

    /// <summary>
    /// The two paddles for the players present in the game.
    /// 
    /// The paddles are added by each player themselves to the apt location in the array i.e. based on their side.
    /// 0:Left, 1:Right
    /// </summary>
    public PaddleController[] paddles = new PaddleController[2];

    public Ball ball;

    public State state;

    public GameObject gameplay, level;
    public GameObject[] levelPrefabs;

    //Network:

    public MyNetworkManager networkManager;

    public MyNetworkDiscovery networkDiscovery;


    #endregion

    #region Unity Defaults

    void Awake(){
        manager = this;
    }

    void Start(){
        //Username
        if (PlayerPrefs.HasKey("name")){
            SetInputName(PlayerPrefs.GetString("name"), false);
        }
    }

    #endregion

    #region UI Manager Buttons

    /// <summary>
    /// Host a new game with the provided level.- called via UI buttons.
    /// </summary>
    /// <param name="lvl"></param>
    public void Host(int lvl){
        //Start broadcasting and host the game with username as the game name
        networkDiscovery.StartBroadcastingAndHost(lvl, userName);

        //Update the state to waiting
        SetState(State.Waiting);

        //Update usernames on the waiting screen based on the side
        //Hosting player get 1st spawn position, 0: left side
        SetUserName(0);
        //Opponent gets 2nd spawn position, 1: right side
        //Also sets temporary username for opp, it is updated with the actual name when the opponent joins
        SetOppName(1, "...");

        //Instantiate the apt level
        InstantiateLevel(lvl);

    }

    /// <summary>
    /// Join a game - called via UI buttons.
    /// </summary>
    /// <param name="gameName">Used to load the apt level.</param>
    public void Join(string gameName){
        //Join the game
        networkDiscovery.JoinGame();

        //Update the state to waiting
        SetState(State.Waiting);

        //Update usernames on the waiting screen based on the side
        //Joinie gets 2nd spawn position, 1: right side
        SetUserName(1);

        //Instantiate the apt level
        int lvl = int.Parse(gameName.Split('-')[0]);
        InstantiateLevel(lvl);
    }

    #endregion

    /// <summary>
    /// Update the state of the game.
    /// </summary>
    /// <param name="st"></param>
    public void SetState(State st){
        // UIManager.manager.Debug_Text("SetState: " + st);

        state = st;

        //Update UI
        UIManager.manager.SetState(st);

        switch (st){

            case State.Menu:

                break;

            case State.Waiting:

                break;

            case State.InGame:

                //Enable gameplay arena
                gameplay.SetActive(true);

                //Spawn ball - only exists on server
                if (SpawnManager.manager != null && SpawnManager.manager.isActiveAndEnabled){
                    SpawnManager.manager.SpawnBall();
                    SpawnManager.manager.SpawnArtifacts();
                }

                break;

            case State.Over:

                //Destroy level
                if (level != null){
                    Destroy(level);
                }

                //Disable gameplay arena
                gameplay.SetActive(false);

                break;

        }
    }

    /// <summary>
    /// Update player name via input or when loaded from player prefs.
    /// This is stored locally and is used later-on for the game.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="updatePrefs"></param>
    public void SetInputName(string name, bool updatePrefs){
        //Update userName, stored for use when starting a game
        userName = name;

        //Update the input box text in main menu
        UIManager.manager.SetInputName(name);

        //Save to playerprefs for next load
        if (updatePrefs){
            PlayerPrefs.SetString("name", name);
            PlayerPrefs.Save();
        }
    }

    /// <summary>
    /// Update user name for the game based on the apt side.
    /// We already know username from inputname (player input or player prefs)
    /// </summary>
    /// <param name="side"></param>
    public void SetUserName(int side){
        UIManager.manager.SetPlayerName(side, userName);
    }

    /// <summary>
    /// Update opp name based on the side.
    /// </summary>
    /// <param name="side"></param>
    /// <param name="name"></param>
    public void SetOppName(int side, string name){
        // UIManager.manager.Debug_Text("SetOppName: " + name);

        oppName = name;

        UIManager.manager.SetPlayerName(side, name);
    }

    /// <summary>
    /// Start the game. Called on all clients by the 2nd paddle on the server, via RPCStartGame.
    /// </summary>
    public void StartGame(){
        //Called on all the clients
        //Update the state of game
        SetState(State.InGame);

    }

    /// <summary>
    /// Gameover, called when the score is max (from the hook which reports any score updates).
    /// </summary>
    /// <param name="side">Tells the side for declaring a winner.</param>
    public void GameOver(int side){
        SetState(State.Over);

        //Check if we won by comparing user name
        if (paddles[side].userName == userName){
            UIManager.manager.OverText("You won!");
        }
        else{
            UIManager.manager.OverText("You lost!");
        }
    }

    /// <summary>
    /// Instantiate the apt level gameobject. Destroys the previous level if a level is already loaded.
    /// </summary>
    /// <param name="lvl"></param>
    public void InstantiateLevel(int lvl){

        //Destroy previous level if any
        if (level != null)
            Destroy(level);

        //Instantiate apt level
        level = Instantiate(levelPrefabs[lvl - 1]);
        level.transform.parent = gameplay.transform;

    }

}
