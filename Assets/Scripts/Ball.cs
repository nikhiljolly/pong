﻿using Pong;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Ball : NetworkBehaviour{

    #region Variables

    //Public

    /// <summary>
    /// Speed of the ball used to set velocity along with an apt direction.
    /// </summary>
    public float speed = 10f, maxSpeed;

    /// <summary>
    /// The side of paddle that last hit the ball.
    /// </summary>
    public int lastPaddleSide = -1;

    //Powers

    /// <summary>
    /// A paddle to which the ball is attached, when using the sticky paddle.
    /// 
    /// Ball follows this paddle when it's not null.
    /// </summary>
    public Rigidbody2D stickyPaddle;

    //Private
    Rigidbody2D body2d;

    //Powers
    /// <summary>
    /// Initial scale of the ball, used to reset the scale.
    /// </summary>
    Vector2 initScale;

    /// <summary>
    /// SyncVar representing current scale of the ball, attached to the hook UpdateSize.
    /// </summary>
    [SyncVar(hook = "UpdateSize")]
    Vector2 currScale;

    /// <summary>
    /// OffsetY of the ball wrt the paddle, when the ball is attached to it.
    /// </summary>
    float stickyOffsetY;

    /// <summary>
    /// Is ball currently teleporting?
    /// 
    /// To maintain a cooldown once teleportation starts. Helps in avoiding teleportation back and forth.
    /// </summary>
    bool isTeleporting;

    #endregion

    #region Unity Defaults

    void Awake(){
        body2d = GetComponent<Rigidbody2D>();
        currScale = initScale = transform.localScale;
    }

    IEnumerator Start(){

        GameManager.manager.ball = this;

        if (isServer){
            //Start moving the ball after 1 sec, only on the server
            yield return new WaitForSeconds(1);
            Movement(Vector2.right * speed);
        }

    }

    #endregion

    #region Physics Defaults and Helpers

    [ServerCallback]
    void FixedUpdate(){
        if (stickyPaddle != null){
            //Follow the paddle
            body2d.position = new Vector2(stickyPaddle.position.x, stickyPaddle.position.y + stickyOffsetY);
        }
    }

    //Runs only on the server: Manages collisions with paddle and exit walls (left and right).
    [ServerCallback]
    void OnTriggerEnter2D(Collider2D other){

        if (stickyPaddle)
            return;

        switch (other.tag){

        //Entered a booster
            case "Booster":

                //Boost velocity, keeping the direction
                //Boosts only till a max speed, to avoid boosting if ball passes through another booster
                if ((body2d.velocity.magnitude * 1.5f) <= maxSpeed)
                    body2d.velocity *= 1.5f;

                break;

        //Entered a shrink zone
            case "Shrink":

                //Update SyncVar which inturn calls hook to update actual scale on clients
                currScale = initScale * 0.6f;

                break;

        //Entered a teleport

            case "Teleport":

                if (!isTeleporting)
                    StartCoroutine(Teleport(0.2f));

                break;

        //Hit a paddle
        //Based on the position of ball wrt paddle, the ball is propelled in the classic pong style
            case "Paddle":

                //Hit a paddle with collider
                HitAPaddle(other);

                break;

            case "Wall":

                //Determine the wall side
                float posX = other.transform.position.x;

                //Hit a wall with side
                HitAWall(posX < 0 ? 0 : 1);

                break;

            case "PowerItem":

                //Power item picked up!
                if (lastPaddleSide != -1)
                    other.GetComponent<PowerItem>().PickedUp(lastPaddleSide);

                break;

        }

    }

    /// <summary>
    /// Teleport the ball: Visually in b/w two teleport pairs.
    /// In reality just mirrors the position of the ball.
    /// 
    /// Also maintains the isTeleporting bool based on the coolDown.
    /// </summary>
    /// <param name="coolDown"></param>
    /// <returns></returns>
    IEnumerator Teleport(float coolDown){

        isTeleporting = true;

        Vector2 oldVel = body2d.velocity;

        //Stop the ball
        Movement(Vector2.zero);

        //Pause for a slight time
        yield return new WaitForSeconds(0.15f);

        //Teleport to new position - mirrored
        body2d.position *= -1;

        //Pause for a slight time
        yield return new WaitForSeconds(0.15f);

        //Start moving the ball with the old velocity
        Movement(oldVel);

        //Cooldown: Wait until the ball succesfully moves outside the teleport zone, avoid teleporting back and forth
        yield return new WaitForSeconds(coolDown);

        isTeleporting = false;
    }

    /// <summary>
    /// Called when a paddle is hit.
    /// Aptly sets the ball behaviour based on the paddle position and power.
    /// </summary>
    /// <param name="paddleCol">The collider of paddle that was hit.</param>
    void HitAPaddle(Collider2D paddleCol){
        //Reset ball scale - change the SyncVar - updates actual scale on clients
        currScale = initScale;

        //Determine the paddle
        var paddle = paddleCol.GetComponentInParent<PaddleController>();

        //Calculate the pos factor based on paddle's and ball's position
        float y = RelativePosFactor(paddle.transform.position, paddleCol.bounds.size.y);

        //Get the direction for the ball
        //Hint: If left paddle, then ball repels in (+1) for x axis
        Vector2 dir = new Vector2(paddle.side == 0 ? 1 : -1, y).normalized;

        //Check if paddle has any powers
        switch (paddle.power){
            case Power.none:
            case Power.increaseSize:

                //Update the velocity of the ball
                Movement(dir * speed);

                break;

            case Power.increaseHitPower:

                //Update the velocity of the ball with a multiplier
                //Velocity auto reset on hitting another paddle, as it is set again
                Movement(dir * speed * 1.5f);

                //Deactivate power - can be only used once
                paddle.SetPower(Power.none);

                break;

            case Power.makeSticky:

                //Stick the ball to the paddle (stop moving the ball) and propel after a delay
                //Deactivates power after usage
                StartCoroutine(StickyBall(paddle,
                        dir * speed, RelativePosFactor(paddle.transform.position, 1f)));


                break;
        }

        //Update last paddle
        lastPaddleSide = paddle.side;

    }

    /// <summary>
    /// Calculates the relative position factor of this ball's y position with the paddle.
    /// </summary>
    /// <param name="padPos">Position of the paddle.</param>
    /// <param name="padHeight">Height of the paddle.</param>
    /// <returns></returns>
    float RelativePosFactor(Vector2 padPos, float padHeight){

        return (transform.position.y - padPos.y) / padHeight;

    }

    /// <summary>
    /// Called when a wall is hit.
    /// 
    /// Respawns/serves the ball, scores the paddle and spwans a power item if the game is not over.
    /// Destroys the ball if game is over.
    /// </summary>
    /// <param name="side">The side on which the wall is.</param>
    void HitAWall(int side){
        //The paddle that gets the score
        int paddleSide = side == 0 ? 1 : 0;

        //Update Score - Opp player gets 1 point
        //Result tells if game is over or not
        if (GameManager.manager.paddles[paddleSide].AddScore(1)){
            //Gameover: Destroy ball
            Destroy(gameObject);
        }
        else{
            //Game not over: Respawn ball

            //The ball is served to the other player i.e. velocity towards the player that lost the point
            //left paddle(0), ball to the right(+1); right paddle(1), ball to the left(-1).
            int dirX = paddleSide == 0 ? 1 : -1;

            //Respawns to init position after 1 sec, providing velocity after a delay of 2 secs
            //Velocity based on the new dir
            StartCoroutine(Respawn((Vector2.right * dirX * speed), 1));

            //Spawn a power item
            SpawnManager.manager.SpawnPowerItem();
        }

    }

    /// <summary>
    /// Respawn the ball to initial position.
    /// And after some delay serve the ball with velocity vel.
    /// </summary>
    /// <param name="vel">The velocity to serve the ball.</param>
    /// <param name="delay">The delay after which the ball starts moving.</param>
    /// <returns></returns>
    IEnumerator Respawn(Vector2 vel, float delay){

        //Reset last paddle side to avoid pickup
        lastPaddleSide = -1;

        //Wait for 1s so that the ball can pass outside the view (looks better) before respawn
        yield return new WaitForSeconds(1f);

        //Reset ball scale - change the SyncVar - updates scale on clients
        currScale = initScale;

        //So that the scale is properly applied - doesn't mess with the rigidbody
        //Note: We are using body2d.position to assign new positions
        yield return new WaitForFixedUpdate();

        //Spawn to init pos with zero vel
        Movement(Vector2.zero, Vector2.zero);

        //Wait for the delay and start moving the ball afterwards (serve the ball)
        yield return new WaitForSeconds(delay);

        Movement(vel);

    }

    /// <summary>
    /// Update the ball's velocity.
    /// </summary>
    /// <param name="vel"></param>
    void Movement(Vector2 vel){
        body2d.velocity = vel;
        //Note: Actually updated at next PhysicsUpdate
    }

    /// <summary>
    /// Update the ball's velocity and position both
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="vel"></param>
    void Movement(Vector2 pos, Vector2 vel){

        body2d.position = pos;
        body2d.velocity = vel;
        //Note: Actually updated at next PhysicsUpdate
    }

    /// <summary>
    /// Stick the ball to the paddle (stop moving the ball).
    /// </summary>
    /// <param name="paddle"></param>
    /// <param name="vel"></param>
    /// <param name="offsetY"></param>
    /// <returns></returns>
    IEnumerator StickyBall(PaddleController paddle, Vector2 vel, float offsetY){
        //Sticky:
        //Stop the ball
        body2d.velocity = Vector2.zero;
        //Stick it: Set sticky paddle, positon updated in FixedUpdate
        stickyPaddle = paddle.GetComponent<Rigidbody2D>();
        //Set the offset of ball wrt paddle position
        stickyOffsetY = offsetY;

        //Wait until the stickyPaddle is set to null - the ball is released on players command
        yield return new WaitUntil(() => stickyPaddle == null);

        //Start moving the ball
        body2d.velocity = vel;

    }

    #endregion

    #region Hook calls

    /// <summary>
    /// Update the scale of the ball on clients when currScale is changed by the server.
    /// </summary>
    /// <param name="newScale"></param>
    void UpdateSize(Vector2 newScale){
        currScale = transform.localScale = newScale;
    }

    #endregion

    /// <summary>
    /// Called when the player invokes a Command on server to release the ball.
    /// Note: The ball is released OnMouseUp.
    /// </summary>
    [Server]
    public void Unstick(){
        stickyPaddle = null;
    }

}
