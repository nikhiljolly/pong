﻿using System.Collections;
using Pong;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// The paddle of a player. Handles input and powers of the paddle.
/// Also manages username and score of the paddle.
/// 
/// Note: Send interval delay decreased, to 0.05f, better input response.
/// A minor optimization is implemented in input to reduce the amount of updates that are sent.
/// </summary>
[NetworkSettings(sendInterval = 0.05f)]
public class PaddleController : NetworkBehaviour{

    #region Variables

    //Public

    /// <summary>
    /// The side of the player to whom the paddle belongs - 0:Left or 1:Right.
    /// </summary>
    public int side;

    /// <summary>
    /// The username of the player to whom the paddle belongs.
    /// A SyncVar using hook UpdateUserName.
    /// </summary>
    [SyncVar(hook = "UpdateUserName")]
    public string userName;

    /// <summary>
    /// The current power of the paddle.
    /// </summary>
    [SyncVar(hook = "UpdatePower")]
    public Power power = Power.none;

    /// <summary>
    /// The score of the player to whom the paddle belongs
    /// </summary>
    [SyncVar(hook = "UpdateScore")]
    public int score;

    /// <summary>
    /// Move/Send input only if difference b/w new pos and old pos is >= inputPrecision.
    /// </summary>
    public float inputPrecision = 0.1f;

    //Private

    /// <summary>
    /// Position of the paddle in Y axis, updated via client inputs.
    /// A SyncVar using hook UpdatePos to update actual position on clients.
    /// </summary>
    [SyncVar(hook = "UpdatePos")]
    float posY;
    float oldPosY;

    /// <summary>
    /// The bounds in which player is allowed to consume input.
    /// Assigned once the side is determined.
    /// </summary>
    BoxCollider2D inputBounds;

    /// <summary>
    /// Score required to win the game - fetched from GameManager
    /// </summary>
    int winScore;

    Vector3 initScale;

    Rigidbody2D body2d;

    #endregion

    #region Unity Defaults

    void Awake(){
        body2d = GetComponent<Rigidbody2D>();

        winScore = GameManager.manager.winScore;
        initScale = transform.localScale;
    }

    void Start(){

        //Determine the paddle's side through spawned position
        side = transform.position.x < 0 ? 0 : 1;

        //Assign in GameManager based on side
        GameManager.manager.paddles[side] = this;

        //Set input bounds based on the side (InputBounds1 or InputBounds2)
        inputBounds = GameObject.FindGameObjectWithTag("InputBounds" + (side + 1)).GetComponent<BoxCollider2D>();

        //If this object doesn't belong to the local player
        if (!isLocalPlayer){
            //Manually update the opponent's name
            //When using SyncVar opponent's name is not updated in case of a late joinie(i.e. the 2nd player)
            GameManager.manager.SetOppName(side, userName);

            //If this is the 2nd player's object that is running on the server
            if (isServer){
                //Send RPC to clients to start game after 2secs of delay
                //Note: Will be called on the corresponding game object i.e. 2nd player gameobject on both the clients
                RpcStartGame(2);
            }
        }

    }

    //Server authoritative input while the game state is InGame
    void Update(){

        //Process only local player input
        if (!isLocalPlayer)
            return;

        //Discard input if game state is not InGame
        if (GameManager.manager.state != State.InGame)
            return;

        //Input
        //If mouse tap or drag occurs - move paddle to the specific position
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0)){
            //Input position
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            //If input taken inside bounds
            //And if difference b/w pos and old pos is >=0.1f - Minor optimization
            if (inputBounds.OverlapPoint(pos) && (Mathf.Abs(pos.y - oldPosY) >= inputPrecision)){
                //Send command to server with the new input position
                CmdMovePos(pos.y);
                oldPosY = pos.y;
            }
        }
        else if (Input.GetMouseButtonUp(0)){

            //Power: Sticky - Release the ball on mouse up
            if (power == Power.makeSticky){
                //Deactivate power
                //Command to unstick the ball on the server
                //Only runs on the server when the ball is actually sticked to the paddle
                CmdUnstick();
            }

        }

    }

    #endregion

    //Callback when the local player completes setup, of-course only on the local player
    public override void OnStartLocalPlayer(){
        //Send command to update the local player's name on the respective server object
        CmdMyName(GameManager.manager.userName);
    }

    #region Hook calls (and helpers)

    /// <summary>
    /// Hook call associated with the username of paddle.
    /// Used mainly to update the opponent's name on both clients.
    /// </summary>
    /// <param name="name"></param>
    void UpdateUserName(string name){
        //UIManager.manager.Debug_Text("UpdateUserName: " + name);

        //When using a hook value is not changed?
        userName = name;

        //If hook called on the other player's object
        if (!isLocalPlayer){
            GameManager.manager.SetOppName(side, userName);
        }
    }

    /// <summary>
    /// Hook associated with the score, called on the respective paddle object on the clients.
    /// Updates the score in UI and also checks if game is over on both clients.
    /// </summary>
    /// <param name="sc"></param>
    void UpdateScore(int sc){
        //print("UpdateScore: " + sc);

        score = sc;

        //Update score of the player as per the side
        UIManager.manager.Score(side, sc);

        //Game over if score is 5
        //Hook call, called atleast on one client whose score becomes 5
        //The winner paddle receives the hook call and initiates gameover
        if (score == winScore){
            GameManager.manager.GameOver(side);
        }

    }

    /// <summary>
    /// Hook on the posY varibale, updates the actual position of the respective paddle object on the clients.
    /// </summary>
    /// <param name="newPosY"></param>
    void UpdatePos(float newPosY){

        posY = newPosY;

        //Move position uses interpolation - paddle will not teleport if interpolation enabled
        body2d.MovePosition(new Vector2(body2d.position.x, posY));

        //Note: Actually updated at next PhysicsUpdate
    }

    /// <summary>
    /// Hook on the power variable, updates the power of the paddle on the clients.
    /// Usually called when the ball collects a power item.
    /// </summary>
    /// <param name="p"></param>
    void UpdatePower(Power p){
        //UIManager.manager.Debug_Text("UpdatePower" + side + " " + p + " from " + power);

        power = p;

        switch (p){
            case Power.none:

                //Reset
                ResetPowers();

                break;

            case Power.increaseSize:

                //Increase the size in Y by multiplier and reset after 1 sec
                StartCoroutine(IncreaseSize(1.3f));

                break;

            case Power.increaseHitPower:

                IncreaseHitPower();

                break;

            case Power.makeSticky:

                MakeSticky();

                break;
        }

    }

    //Powers

    //None

    /// <summary>
    /// Called when the power of the paddle is set to none.
    /// Reset the powers, as of now resets the size and color. Other powers don't have any physical affect.
    /// </summary>
    void ResetPowers(){
        ResetSize();
        //Other powers are not physical changes to the paddle

        //Reset color for better recoginition
        GetComponent<SpriteRenderer>().color = Color.white;

    }

    /// <summary>
    /// Reset the size of the paddle to initial scale.
    /// </summary>
    void ResetSize(){
        //Reset size
        transform.localScale = new Vector3(initScale.x, initScale.y, initScale.z);
    }

    //Size

    /// <summary>
    /// Called when the power of the paddle is set to increseSize.
    /// Increase the sizeY of paddle by a multiplier whilst changing the color of the paddle.
    /// Also resets the power afer 5s.
    /// </summary>
    /// <param name="multiplierY"></param>
    /// <returns></returns>
    IEnumerator IncreaseSize(float multiplierY){
        //Increase scale in y direction
        transform.localScale = new Vector3(initScale.x, multiplierY * initScale.y, initScale.z);

        //Change color for better recoginition
        GetComponent<SpriteRenderer>().color = Color.blue;

        yield return new WaitForSeconds(5);

        //Reset sync var on the server after 5 secs, inturn calls the hook
        //Only do that if power not already changed - only one power is active at a time
        if (isServer && power == Power.increaseSize)
            power = Power.none;

    }

    //Hit Power

    /// <summary>
    /// Called when the power of the paddle is set to increaseHitPower.
    /// No physical changes besides color.
    /// </summary>
    void IncreaseHitPower(){
        //If previously size was large - only one power is to be active
        ResetSize();

        //Hit power doesn't make any phyical change
        //Ball automatically checks for the power state, and reacts appropriately.

        //Change color for better recoginition
        GetComponent<SpriteRenderer>().color = Color.red;
    }


    //Sticky
    /// <summary>
    /// Called when the power of the paddle is set to makeSticky.
    /// No physical changes besides color.
    /// </summary>
    void MakeSticky(){
        //If previously size was large - only one power is to be active
        ResetSize();

        //Sticky power doesn't make any phyical change
        //Ball automatically checks for the power state, and reacts appropriately.

        //Change color for better recoginition
        GetComponent<SpriteRenderer>().color = Color.grey;
    }

    #endregion

    #region Remote calls

    /// <summary>
    /// Start the game with some delay, runs on clients.
    /// Usually called via 2nd player object on the server.
    /// </summary>
    /// <param name="delay"></param>
    [ClientRpc]
    void RpcStartGame(float delay){
        //print("RpcStartGame: "+gameObject);

        //Start game after some delay
        GameManager.manager.Invoke("StartGame", delay);
    }

    /// <summary>
    /// Updates the SyncVar posY on the server.
    /// Which inturn calls the hook "UpdatePos" on the clients to update the actual position
    /// </summary>
    /// <param name="newPosY"></param>
    [Command]
    void CmdMovePos(float newPosY){
        //Set position on the server
        //Inturn calls hook
        posY = newPosY;
    }

    /// <summary>
    /// Updates the name to a SyncVar corresponding to the respective player object on the server.
    /// Called by a local player passing his own name
    /// </summary>
    /// <param name="name"></param>
    [Command]
    void CmdMyName(string name){
        //Update name on server object
        userName = name;
    }

    /// <summary>
    /// Command from a client/player paddle to unstick the ball on the corresponding server object.
    /// Note: Called OnMouseUp.
    /// </summary>
    [Command]
    public void CmdUnstick(){
        //Check if ball is currently sticked to this paddle or not
        //Allows the player to use the power atleast once
        if (GameManager.manager.ball.stickyPaddle == body2d){
            //Unstick the ball on server
            GameManager.manager.ball.Unstick();

            //Update the power to none
            SetPower(Power.none);
        }
    }

    #endregion

    #region External - Called from outside

    /// <summary>
    /// Called on server when a ball hits a wall.
    /// Adds to score, hook call UpdateScore is called automagically which initiates game over if score is max.
    /// Returns true if game is over, used to destroy ball or respawn it if game is not over.
    /// </summary>
    /// <param name="add"></param>
    /// <returns></returns>
    [Server]
    public bool AddScore(int add){
        score += add;

        //If score is 5 game over, returns true
        if (score == winScore){
            return true;
        }

        return false;
    }


    //Powers

    /// <summary>
    /// Set the power of paddle, updates the SyncVar.
    /// Called on the server by the ball or paddle.
    /// </summary>
    /// <param name="p"></param>
    [Server]
    public void SetPower(Power p){
        power = p;
    }

    #endregion

}
