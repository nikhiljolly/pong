﻿using Pong;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour{

    #region Variables

    public static UIManager manager;

    //Main Menu

    /// <summary>
    /// The input name represented by the input field in the main menu.
    /// </summary>
    public InputField inputName;

    /// <summary>
    /// A game list that shows the available games that can be joined.
    /// </summary>
    public ScrollRect gameList;

    /// <summary>
    /// Prefab for creating a game inside the gamelist.
    /// </summary>
    public GameObject joinButtonPrefab;

    //Waiting
    public Text waitingTitle;
    /// <summary>
    /// The two player names, updated once both the player join.
    /// </summary>
    public Text[] playerNames;

    //InGame
    public Text[] scores;

    //Over
    public Text overText;

    //Debug
    public GameObject debug;
    public Text debugText;

    //Private
    Animator animator;

    #endregion

    #region Unity Defaults

    void Awake(){
        manager = this;
        animator = GetComponent<Animator>();

    }

    #endregion

    #region State

    /// <summary>
    /// Update the state of UI based on game state.
    /// </summary>
    /// <param name="st"></param>
    public void SetState(State st){
        // print("SetState: " + st);

        switch (st){

            case State.Menu:

                animator.SetTrigger("menu");

                break;

            case State.Waiting:

                animator.SetTrigger("waiting");

                break;

            case State.InGame:

                animator.SetTrigger("ingame");

                break;

            case State.Over:

                animator.SetTrigger("over");

                break;

        }
    }

    #endregion

    #region Buttons Clicked

    //Menu
    /// <summary>
    /// Host the game with apt level, called by Host 1 and 2 buttons.
    /// </summary>
    /// <param name="lvl"></param>
    public void Host(int lvl){
        GameManager.manager.Host(lvl);

        WaitingText("Waiting for an opponent to join..");
    }

    /// <summary>
    /// Join the game associated with the gamename, called by the join game button.
    /// </summary>
    /// <param name="gameName">The name received from the broadcast data.</param>
    public void Join(string gameName){
        GameManager.manager.Join(gameName);

        WaitingText("Starting the game..");
    }

    /// <summary>
    /// Called when a new name is entered in the input field.
    /// Update the input (local) username. Not used in the game as of now, will be used once a game is hosted.
    /// </summary>
    /// <param name="name"></param>
    public void InputName(string name){
        print("InputName: " + name);

        //Update input (local) username
        GameManager.manager.SetInputName(name, true);
    }

    #endregion

    #region Debug

    public void Debug_Text(string txt){
        print(txt);
        debugText.text = txt;
    }

    //Used to reset the game, hard reset clears the player prefs as well.
    public void Reset(bool hard){
        if (hard)
            PlayerPrefs.DeleteAll();

        Destroy(GameManager.manager.networkManager.gameObject);
        MyNetworkManager.Shutdown();

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    #endregion

    #region UI External

    /// <summary>
    /// Update the UI-score of the player with respect to the side.
    /// </summary>
    /// <param name="side"></param>
    /// <param name="score"></param>
    public void Score(int side, int score){
        // print("Score: " + side + " | " + score);
        scores[side].text = "" + score;
    }

    /// <summary>
    /// Update the UI-game over text.
    /// </summary>
    /// <param name="text"></param>
    public void OverText(string text){
        overText.text = text;
    }

    /// <summary>
    /// Update the UI - Player name in the waiting screen.
    /// Side - 0:Left, 1:Right.
    /// </summary>
    /// <param name="side"></param>
    /// <param name="name"></param>
    public void SetPlayerName(int side, string name){
        // print("SetPlayerName: " + side + " | " + name);
        playerNames[side].text = name;
    }

    /// <summary>
    /// Update the UI - input name field. Deosn't change the username anywhere else.
    /// </summary>
    /// <param name="name"></param>
    public void SetInputName(string name){
        inputName.text = name;
    }

    /// <summary>
    /// Add a join game button into the game list, with the name.
    /// </summary>
    /// <param name="name">The name forwarded in the broadcast data.</param>
    public void AddAvailableGame(string name){

        //Instantiate button
        GameObject button = Instantiate(joinButtonPrefab) as GameObject;

        //Add to the game list
        button.transform.SetParent(gameList.content, false);

        //Set name of the game
        button.GetComponentInChildren<Text>().text = name + "'s game";

        //Add Join() method along with the broadcast data to the on click listener
        button.GetComponent<Button>().onClick.AddListener(() => Join(name));

        //Remove button after broadcast interval expires
        Destroy(button, GameManager.manager.networkDiscovery.broadcastInterval / 1000);

    }

    /// <summary>
    /// Update the waiting text on the waiting screen.
    /// </summary>
    /// <param name="text"></param>
    public void WaitingText(string text){
        waitingTitle.text = text;
    }

    #endregion

}
