﻿using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// The SpawnManager that only exists on the Server.
/// Handles spawning the ball, a power item and artifacts.
/// </summary>
public class SpawnManager : NetworkBehaviour{
    public static SpawnManager manager;

    #region Variables

    public GameObject ballPrefab, powerItemPrefab, artifactPrefab;

    #endregion

    #region Unity Defaults

    void Awake(){
        manager = this;
    }

    #endregion

    /// <summary>
    /// Spawn the ball.
    /// </summary>
    [Server]
    public void SpawnBall(){
        //Create an instance of the ball
        var ball = Instantiate(ballPrefab);

        //Spawn on clients
        NetworkServer.Spawn(ball);

        //Ball's velocity is automatically set in start for now

    }

    /// <summary>
    /// Spawn a power item in the middle of the screen. Power is randomly chosen in Awake().
    /// If a power item is already present, just regenerate the power.
    /// </summary>
    [Server]
    public void SpawnPowerItem(){
        //No power item present
        if (PowerItem.activePowerItem == null){
            //Create an instance of power item
            var powerItem = Instantiate(powerItemPrefab);

            //Spawn on clients
            NetworkServer.Spawn(powerItem);
        }
        else{
            //Power item already present
            //Just regenarate a new power
            PowerItem.activePowerItem.Regenerate();
        }

    }

    /// <summary>
    /// Spawn 2 artifacts.
    /// </summary>
    [Server]
    public void SpawnArtifacts(){
        var artifact1 = Instantiate(artifactPrefab, new Vector2(0, 2), Quaternion.identity);
        var artifact2 = Instantiate(artifactPrefab, new Vector2(0, -2), Quaternion.identity);

        NetworkServer.Spawn(artifact1);
        NetworkServer.Spawn(artifact2);
    }

}
